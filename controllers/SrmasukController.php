<?php

namespace app\controllers;

use Yii;
use app\models\Srmasuk;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\UploadedFile;
use yii\filters\AccessControl;

/**
 * SrmasukController implements the CRUD actions for Srmasuk model.
 */
class SrmasukController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access'=> [
                'class'=>AccessControl::className(),
                'only'=>['create','index','update','view'],
                'rules'=>[
                    [
                        'allow'=>true,
                        'roles'=>['@']
                    ]
                ]
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Srmasuk models.
     * @return mixed
     */
    public function actionIndex()
    {
        $dataProvider = new ActiveDataProvider([
            'query' => Srmasuk::find(),
        ]);

        return $this->render('index', [
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Srmasuk model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Srmasuk model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Srmasuk();
        $model->scenario = 'create';
        if ($model->load(Yii::$app->request->post())) {
            $model->file_masuk = UploadedFile::getInstance($model, 'file_masuk');

            if($model->validate()){
                $fileName=uniqid($model->no_surat);
                Yii::$app->db->createCommand()->insert('id_srmasuk', [
                    'no_surat' => $model->no_surat,
                    'nama_surat' => $model->nama_surat,
                    'tujuan_id' => $model->tujuan_id,
                    'tgl_surat' => $model->tgl_surat,
                    'catatan' => $model->catatan,
                    'file_masuk' => $fileName.'.'.$model->file_masuk->extension,
                ])->execute();
                $model->id = Yii::$app->db->getLastInsertID();
                $model->file_masuk->saveAs('uploads/'.$fileName.'.'.$model->file_masuk->extension);
                $model->file_masuk = 'file_masuk.' . $fileName;
                return $this->redirect(['view', 'id' => $model->id]);
            }
            
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        $model->scenario = 'update';
        if ($model->load(Yii::$app->request->post())) {
            $model->file_masuk = UploadedFile::getInstance($model, 'file_masuk');

            if($model->validate()){
              if(isset($model->file_masuk))
              {
                $fileName=uniqid($model->no_surat);
                Yii::$app->db->createCommand()->update('id_srmasuk', [
                    'no_surat' => $model->no_surat,
                    'nama_surat' => $model->nama_surat,
                    'tujuan_id' => $model->tujuan_id,
                    'tgl_surat' => $model->tgl_surat,
                    'catatan' => $model->catatan,
                    'file_masuk' => $fileName.'.'.$model->file_masuk->extension,
                ],['id'=>$model->id])->execute();
                $model->file_masuk->saveAs('uploads/'.$fileName.'.'.$model->file_masuk->extension);
                $model->file_masuk = 'file_masuk.' . $fileName;
              }
              else
              {
                Yii::$app->db->createCommand()->update('id_srmasuk', [
                    'no_surat' => $model->no_surat,
                    'nama_surat' => $model->nama_surat,
                    'tujuan_id' => $model->tujuan_id,
                    'tgl_surat' => $model->tgl_surat,
                    'catatan' => $model->catatan,
                ],['id'=>$model->id])->execute();
              }
                
                return $this->redirect(['view', 'id' => $model->id]);
            }
            
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing Srmasuk model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
   

    /**
     * Deletes an existing Srmasuk model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Srmasuk model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Srmasuk the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Srmasuk::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}


// note update cadangan

 // public function actionUpdate($id)
    // {
    //     $model = $this->findModel($id);

    //     if(isset($_POST['delete_file']))
    //     {
    //         Yii::$app->db->createCommand()->update('id_srmasuk', [
    //             'file_masuk' => ''],['id'=>$model->id])->execute();
    //         return $this->refresh();
    //     }

    //     if ($model->load(Yii::$app->request->post())) {

    //       if($_FILES['file_masuk']['error']==4)
    //       {
    //         if($model->validate())
    //         {
    //             Yii::$app->db->createCommand()->update('id_srmasuk', [
    //                     'no_surat' => $model->no_surat,
    //                     'nama_surat' => $model->nama_surat,
    //                     'tujuan_id' => $model->tujuan_id,
    //                     'catatan' => $model->catatan,
    //                 ],['id'=>$model->id])->execute();
    //                 return $this->redirect(['view', 'id' => $model->id]);
    //         }
    //       }  
    //       else
    //       {
    //         $model->file_masuk = UploadedFile::getInstance($model, 'file_masuk');

    //         if($model->validate())
    //         {
    //             $tmpfile=$_FILES['file_masuk']['tmp_name'];
    //             $exfile=explode(".", $_FILES['file_masuk']['name']);
    //             $exfile=".".end($exfile);
    //             $newname=uniqid($model->no_surat);
    //             $file=$newname.$exfile;

    //             $sql= Yii::$app->db->createCommand()->update('id_srmasuk', [
    //                 'no_surat' => $model->no_surat,
    //                 'nama_surat' => $model->nama_surat,
    //                 'tujuan_id' => $model->tujuan_id,
    //                 'catatan' => $model->catatan,
    //                 'file_masuk' => $file,
    //             ],['id'=>$model->id])->execute();
    //             move_uploaded_file($tmpfile, 'uploads/'.$file);
    //             $model->file_masuk = 'file_masuk.' . $newname;
    //             return $this->redirect(['view', 'id' => $model->id]);
    //         }
    //       }
            
    //     }

    //     return $this->render('update', ['model' => $model]);
    // }