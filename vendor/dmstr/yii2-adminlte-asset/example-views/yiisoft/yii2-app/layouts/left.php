<aside class="main-sidebar" style="position: fixed;">

    <section class="sidebar">

        <!-- Sidebar user panel -->
        <div class="user-panel">
            <div class="pull-left image">
                <img src="<?= $directoryAsset ?>/img/user.jpg" class="img-circle" alt="User Image"/>
            </div>
            <div class="pull-left info">
                <p><?= Yii::$app->user->identity->profil_name ?></p>

                <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
            </div>
        </div>

        <!-- search form -->
<!--         <form action="#" method="get" class="sidebar-form">
            <div class="input-group">
                <input type="text" name="q" class="form-control" placeholder="Search..."/>
              <span class="input-group-btn">
                <button type='submit' name='search' id='search-btn' class="btn btn-flat"><i class="fa fa-search"></i>
                </button>
              </span>
            </div>
        </form> -->
        <!-- /.search form -->

        <?= dmstr\widgets\Menu::widget(
            [
                'options' => ['class' => 'sidebar-menu tree', 'data-widget'=> 'tree'],
                'items' => 
                [
                    [
                        'label' => 'Letter',
                        'icon' => 'envelope',
                        'url' => '#',
                        'items' => 
                        [
                            ['label' => 'Incoming', 'icon' => 'share', 'url' => ['/srmasuk'],
                                'active' => in_array($this->context->route,['srmasuk/index','srmasuk/create','srmasuk/update','srmasuk/view'])
                            ],
                            ['label' => 'Outgoing', 'icon' => 'reply', 'url' => [''],],
                        ],
                    ],
                    [
                        'label' => 'Settings',
                        'icon' => 'gear',
                        'url' => '#',
                        'items' => 
                        [
                            ['label' => 'Department', 'icon' => 'building', 'url' => ['/department'],
                                'active' => in_array($this->context->route,['department/index','department/create','department/update','department/view'])
                            ],
                            ['label' => 'User', 'icon' => 'user', 'url' => ['/user'],
                                'active' => in_array($this->context->route,['user/index','user/create','user/update','user/view'])
                            ],
                        ],
                    ],
                ],

            ]
        ) ?>

    </section>

</aside>
