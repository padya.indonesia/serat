<?php
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \common\models\LoginForm */

$this->title = 'Sign In';

$fieldOptions1 = [
    'options' => ['class' => 'form-group has-feedback'],
    // 'inputTemplate' => "{input}<span class='glyphicon glyphicon-envelope form-control-feedback'></span>"
];

$fieldOptions2 = [
    'options' => ['class' => 'form-group has-feedback'],
    // 'inputTemplate' => "{input}<span class='glyphicon glyphicon-lock form-control-feedback'></span>"
];
?>

<div class="login-box">
    <div class="login-logo"></div>
    <!-- /.login-logo -->
    <div class="login-box-body">
        <!-- <p class="login-box-msg">Sign in to start your session</p> -->

        <?php $form = ActiveForm::begin(['id' => 'login-form', 'enableClientValidation' => false]); ?>

        <!-- <div class="wrap-input100 validate-input" data-validate = "Valid email is required: ex@abc.xyz">
            <input class="input100" type="text" name="email" placeholder="Email">
            <span class="focus-input100"></span>
            <span class="symbol-input100">
                <i class="fa fa-envelope" aria-hidden="true"></i>
            </span>
        </div> -->
        <?= $form
            ->field($model, 'username', ['template'=>'<div class="wrap-input100 validate-input" data-validate = "Valid email is required: ex@abc.xyz">
                {input}
                <span class="focus-input100"></span>
                <span class="symbol-input100">
                <i class="fa fa-user" aria-hidden="true"></i>
                </span>
                {error}
                </div>'])
            ->label(false)
            ->textInput(['class'=>'input100','autofocus'=>'autofocus','placeholder' => $model->getAttributeLabel('username')]) ?>

        <?= $form
            ->field($model, 'password', ['template'=>'<div class="wrap-input100 validate-input" data-validate = "Password is required">
                {input}
                <span class="focus-input100"></span>
                <span class="symbol-input100">
                <i class="fa fa-lock" aria-hidden="true"></i>
                </span>
                {error}
                </div>'])
            ->label(false)
            ->passwordInput(['class'=>'input100','placeholder' => $model->getAttributeLabel('password')]) ?>

       <div class="container-login100-form-btn">
            <button class="login100-form-btn" name="login-button" type="submit">
                Login
            </button>
        </div>

        <?php ActiveForm::end(); ?>

       <!--  <a href="#">I forgot my password</a><br> -->

    </div>
    <!-- /.login-box-body -->
</div><!-- /.login-box -->
