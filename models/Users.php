<?php

namespace app\models;

use Yii;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "id_user".
 *
 * @property int $id
 * @property string $profil_name
 * @property string $username
 * @property string $password
 */
class Users extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'id_user';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['profil_name', 'username'], 'required'],
            ['password', 'required', 'on' => 'create'],
            [['profil_name', 'username', 'password','authKey','accessToken','lastLogin'], 'string', 'max' => 100],
            ['username', 'unique'],
        ];
    }

    public function beforeSave($options = array()) {

        $authKey = md5($this->username);
        $accessToken = md5($this->username);

        if(empty($_POST['Users']['password']))
        {
            $pass = $_POST['oldps'];
        }
        else
        {
            $pass = md5(sha1(md5(sha1($this->password))));
        }
        
        $this->password = $pass;
        $this->authKey = $authKey;
        $this->accessToken = $accessToken;

        return true;
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'profil_name' => 'Profil Name',
            'username' => 'Username',
            'password' => 'Password',
        ];
    }
}
