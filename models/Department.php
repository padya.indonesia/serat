<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "id_department".
 *
 * @property int $id
 * @property string $code
 * @property string $nama_department
 */
class Department extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'id_department';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['code', 'nama_department'], 'required'],
            [['code', 'nama_department','parent'], 'string', 'max' => 100],
            ['code', 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'code' => 'Code',
            'nama_department' => 'Department',
            'parent' => 'Parent',
        ];
    }

    public function getDepartment()
    {
        return $this->hasOne(Department::className(), ['id' => 'parent']);
    }

    

}
