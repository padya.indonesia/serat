<?php

namespace app\models;

use Yii;
use yii\web\UploadedFile;

/**
 * This is the model class for table "id_srmasuk".
 *
 * @property int $id
 * @property string $no_surat
 * @property string $nama_surat
 * @property string $tujuan_id
 * @property string $catatan
 * @property string $tgl_surat
 * @property string $file_masuk
 */
class Srmasuk extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'id_srmasuk';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['no_surat', 'nama_surat', 'tujuan_id', 'tgl_surat'], 'required'],
            [['catatan'], 'string'],
            [['no_surat', 'nama_surat', 'tujuan_id'], 'string', 'max' => 100],
            [['file_masuk'], 'file', 'extensions' => 'png, jpg, jpeg, pdf','mimeTypes'=>'image/jpeg,image/png,application/pdf', 'skipOnEmpty'=>false, 'on'=>'create'],
            [['file_masuk'], 'file', 'extensions' => 'png, jpg, jpeg, pdf','mimeTypes'=>'image/jpeg,image/png,application/pdf', 'skipOnEmpty'=>true, 'on'=>'update'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'no_surat' => 'Ref. Number',
            'nama_surat' => 'Letter Name',
            'tujuan_id' => 'Purpose',
            'catatan' => 'Note',
            'tgl_surat' => 'Date',
            'file_masuk' => 'File',
        ];
    }

    public function getDepartment()
    {
        return $this->hasOne(Department::className(), ['id' => 'tujuan_id']);
    }

}
