<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Department';
// $this->params['breadcrumbs'][] = $this->title;
?>
<div class="department-index">

    <div class="row">
        <div class="col-sm-11">
            <h1><?= Html::encode($this->title) ?></h1>
        </div>
        <div class="col-sm-1"></div>
    </div>
    <p>
        <?= Html::a('<i class="fa fa-fw fa-plus"></i> Create', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'formatter'=>['class'=>'yii\i18n\Formatter','nullDisplay'=>''],
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            ['header'=>'Code','attribute'=>'code'],
            ['header'=>'Department','attribute'=>'nama_department'],
            [
                'header'=>'Parent',
                'value'=>'department.nama_department',
            ],
            [
                'class' => 'yii\grid\ActionColumn',
                'header' => 'Action',
                'template' => '{update} {delete}',
            ],
        ],

    ]); ?>


</div>
