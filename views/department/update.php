<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Department */

$this->title = 'Update ';
?>
<div class="id-department-update">

    <h3><?= Html::encode($this->title) ?> <i>#<?= $model->nama_department ?></i></h3>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
