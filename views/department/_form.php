<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;

/* @var $this yii\web\View */
/* @var $model app\models\Department */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="department-form">

    <?php $form = ActiveForm::begin(); ?>

    <div class="box box-danger">
    <div class="box-header with-border"></div>
    <div class="box-body">
        <div class="row">
            <div class="col-xs-2">
                <?= $form->field($model, 'code')->textInput(['maxlength' => 5]) ?>
            </div>
            <div class="col-xs-4">
                <?= $form->field($model, 'nama_department')->textInput(['maxlength' => true]) ?>
            </div>
            <div class="col-xs-4">
                <?= $form->field($model, 'parent')->dropDownList(
			        ArrayHelper::map(\app\models\Department::find()->all(),'id','nama_department'),
			        ['prompt'=>'Without Parent']); ?>
            </div>
        </div>
        <br>
        <div class="form-group">
             <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
            <?= Html::a('Cancel', ['index'], ['class' => 'btn btn-danger']) ?>
        </div> 
    </div>
    <!-- /.box-body -->
    </div>
    <!-- /.box -->

    <?php ActiveForm::end(); ?>

</div>
