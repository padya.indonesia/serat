<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\User */

$this->title = 'Update ';
?>
<div class="user-update">

     <h3><?= Html::encode($this->title) ?> <i>#<?= $model->username ?></i></h3>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
