<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Users */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="users-form">

    <?php $form = ActiveForm::begin(); ?>

    <div class="box box-danger">
    <div class="box-header with-border"></div>
    <div class="box-body">
        <div class="row">
            <div class="col-xs-4">
                <?= $form->field($model, 'profil_name')->textInput(['maxlength' => true]) ?>
            </div>
            <div class="col-xs-4">
               <?= $form->field($model, 'username')->textInput(['maxlength' => true]) ?>
            </div>
            <div class="col-xs-4">
               <?= $form->field($model, 'password')->passwordInput(['value'=>'','maxlength' => true]) ?>
            <?php if(!$model->isNewRecord): ?>
               <?= Html::hiddenInput('oldps', $model->password); ?>
               <small>*if not change the password, please leave it blank</small>
            <?php endif; ?>
            </div>
        </div>
        <div class="form-group">
            <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
            <?= Html::a('Cancel', ['index'], ['class' => 'btn btn-danger']) ?>
        </div> 
    </div>
    <!-- /.box-body -->
    </div>
    <!-- /.box -->

    <?php ActiveForm::end(); ?>

</div>
