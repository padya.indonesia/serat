<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Users';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="user-index">

    <h1>User Management</h1>

    <p>
        <?= Html::a('<i class="fa fa-fw fa-plus"></i> Create', ['create'], ['class' => 'btn btn-success']) ?>
    </p>


    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            ['header'=>'Profil Name','attribute'=>'profil_name'],
            ['header'=>'Username','attribute'=>'username'],
            [
                'label'=>'Last Login',
                'value'=>'lastLogin',
            ],
            [
                'class' => 'yii\grid\ActionColumn',
                'header' => 'Action',
                'template' => '{update} {delete}',
                
            ],
        ],
    ]); ?>


</div>
