<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Srmasuk */

$this->title = 'Archive Letter';
// $this->params['breadcrumbs'][] = ['label' => 'Srmasuks', 'url' => ['index']];
// $this->params['breadcrumbs'][] = $this->title;
?>
<div class="srmasuk-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
