<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use yii\jui\DatePicker;

/* @var $this yii\web\View */
/* @var $model app\models\Srmasuk */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="srmasuk-form">

    <?php $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data','autocomplete'=>'off']]); ?>
    <div class="box box-danger">
    <div class="box-body">
      <div class="row">
        <div class="col-xs-3">
           <?= $form->field($model, 'no_surat')->textInput(['maxlength' => true]) ?>
        </div>
        <div class="col-xs-5">
            <?= $form->field($model, 'nama_surat')->textInput(['maxlength' => true]) ?>
        </div>
        <div class="col-xs-3">
            <?= $form->field($model, 'tujuan_id')->dropDownList(
              ArrayHelper::map(\app\models\Department::find()->all(),'id','nama_department'),
              ['prompt'=>'--Select Department--']); ?>
        </div>
      </div>
      <div class="row">
        <div class="col-xs-2">
          <?= $form->field($model, 'tgl_surat')->widget(DatePicker::className(),['dateFormat' => 'yyyy-MM-dd']) ?>
        </div>
        <div class="col-xs-6">
          <?= $form->field($model, 'catatan')->textarea(['style' => 'resize:none','rows' => 6]) ?>
        </div>
        <div class="col-xs-3">
           <?= $form->field($model, 'file_masuk')->fileInput(); ?>
           <?php if(!$model->isNewRecord): ?>
              <small>*if not change file, please leave it blank</small>
           <?php endif; ?>
           <p class="help-block">Format: PDF/JPG/JPEG/PNG max. 3 MB</p>
        </div>
      </div>
    </div>
    <!-- /.box-body -->
    </div>
    <!-- /.box -->

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
      <?php if($model->file_masuk): ?>
        <?= Html::a('Cancel',['index'], ['class' => 'btn btn-danger']) ?>
      <?php endif; ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
