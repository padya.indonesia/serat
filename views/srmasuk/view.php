<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Srmasuk */

$this->title = $model->no_surat;
\yii\web\YiiAsset::register($this);
?>
<div class="srmasuk-view">

<div class="row">
    <div class="col-xs-9">
       <h3><b>Incoming Mail <i>#<?= Html::encode($this->title) ?></i></b></h3>  
    </div>
    <div class="col-xs-3">
      <p>
        <?= Html::a('<i class="fa fa-fw fa-list"></i> List', ['index'], ['class' => 'btn btn-success']) ?>
        <?= Html::a('<i class="fa fa-fw fa-pencil"></i>Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('<i class="fa fa-fw fa-trash"></i>Delete', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
      </p>  
    </div>
</div>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'no_surat',
            'nama_surat',
            [
                'attribute'=>'tujuan_id',
                'value'=>($model->department)?$model->department->nama_department:"-",
            ],
            [
                'attribute'=>'tgl_surat',
                'value'=> date('d-m-yy', strtotime($model->tgl_surat))
            ],
            'catatan:ntext',
            [
                'format' => 'raw',
                'name'=>'file_masuk','label'=>'File',
                'value'=> Html::a('<i class="fa fa-file-image-o"></i> Preview File','uploads/'.$model->file_masuk,['target'=>'_blank'])
            ],
        ],
    ]) ?>

</div>
