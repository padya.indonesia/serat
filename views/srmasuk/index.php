<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Incoming Mail';
// $this->params['breadcrumbs'][] = $this->title;
?>
<div class="srmasuk-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('<i class="fa fa-fw fa-plus"></i> Create', ['create'], ['class' => 'btn btn-success']) ?>
    </p>


    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            ['attribute'=>'no_surat'],
            ['attribute'=>'nama_surat'],
            [
                'attribute'=>'tgl_surat',
                'value'=>'tgl_surat',
                'format' =>  ['date', 'dd-MM-Y'],
            ],
            [
                'attribute'=>'tujuan_id',
                'value'=>'department.nama_department',
            ],
            [
                'class' => 'yii\grid\ActionColumn',
                'header' => 'Action',
                'template' => '{view} {update} {delete}',
            ],

        ],
    ]); ?>


</div>
